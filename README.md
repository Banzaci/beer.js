# README #

Beerjs is an event driven module architecture, and inspired by Nicholas C. Zakas "Creating A Scalable JavaScript Application Architecture".

To start:

```

var options = {
  storage:[{
      name:'nameOfStorage',
      type:'localStorage'
  }]
};

BEER.CORE.start(options).run();

```

Modules:

Create folder 'modules' and and then create ie 'init.js'

init.js

```

(function(e){
    'use strict';
    e.CORE.subscribe('init', (function( sandbox ){

        var requestResponse = function(response){
            sandbox.notify('eventName', response);
        };

        return {
            init:function(){
                //this.container: the element who has the 'data-module' attribute
                //Rest call:
                sandbox.request().get('/api/v1/', requestResponse.bind(self), {});
            },
            destroy:function(){}
        };

    }));
}( BEER ));

```

another.js

```

(function(e){
    'use strict';
    e.CORE.subscribe('another', (function( sandbox ){

        var eventResponse = function(data){
            console.log(data);//from init.js
        };

        return {
            init:function(){
                //this.container: the element who has the 'data-module' attribute
                sandbox.listen('eventName', eventResponse.bind(self));
            },
            destroy:function(){}
        };

    }));
}( BEER ));

```

In dom, add data-module="init"

```

<div data-module="init"></div>

<div data-module="another"></div>

```

Working with dom objects:

```

<div data-module="another">
  <button class="js-button">Button</button>
</div>

```

```

(function(e){
    'use strict';
    e.CORE.subscribe('another', (function( sandbox ){
        //another.js
        var eventResponse = function(data){
            console.log(data);//from init.js
            this.container.child('js-button').on('click', function(event){
                /*
                For all options on dom element, check dom.js in dist/core/lib/dom.js
                */
            });
        };

        return {
            init:function(){
                //this.container: the element who has the 'data-module' attribute
                sandbox.listen('eventName', eventResponse.bind(self));
            },
            destroy:function(){}
        };

    }));
}( BEER ));

```


Storage

```

var storage = sandbox.storage();
storage.set('key', 'value');
storage.get('key');

```

Extend

If you need to share same code base between modules:

1. create folder extends next to modules folder.

2. create extension:

```

(function(e){
    'use strict';
    e.CORE.extension('extendMe', (function( ){

        return {
            createExtend:function(){
                console.log('I am extended!');
            }
        };
    }));
}( BEER ));

```

In module:

```

(function(e){
    'use strict';
    e.CORE.subscribe('init', (function( sandbox ){

        return {
            init:function(){
                this.extendMe.createExtend();
            },
            destroy:function(){}
        };
    }), 'extendMe');// if wanted, you can include more by comma seperation: 'one, two, .....'
}( BEER ));


```

Create own services:

1. create folder services next to modules

2. create service:

```

//If you need to add custom services to project. Services specific for this project
(function( e ){
    'use strict';
    e.CORE.define('serviceExample', (function(){
        return {
            init:function(){
                console.log('Service extension has been called');
            }
        }
    }));
}( BEER ));

```

In module:

```

(function(e){
    'use strict';
    e.CORE.subscribe('init', (function( sandbox ){

        return {
            init:function(){
                sandbox.service('serviceExample').init();
            },
            destroy:function(){}
        };
    }));
}( BEER ));


```

Validate form (min, max, email, equal, time(12:00) and date)

```

(function(e){
    'use strict';
    e.CORE.subscribe('another', (function( sandbox ){

        return {
            init:function(){
                var validate = element.attr('data-validate');
                var element  = this.container('elementToValidate');
                if( validate ){
                    var conditions  = validate.split(',');
                    if(conditions.length > 0) {
                        var hasErrors = sandbox.validate().check( conditions, element.value(), []);
                        // Will return if input field in this case only contains of 2 letters:
                        // Object { func: "min", hasError: true }
                    }
                }
            },
            destroy:function(){}
        };
    }));
}( BEER ));

<div data-module="another">
    <input type="text" data-validate="min:5, max:20" />//only 2 letters
</div>




```
