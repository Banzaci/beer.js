var BEER = {};
(function( e ){
	'use strict';
	e.CORE = (function(){

		var resizeImage, sandbox, util, log, request, dom, constant, socket, storage, validate, binder;
		var MODULE_SELECTOR = '[data-module]';
		var services 		= {};
		var events  		= {};
		var modules			= {};
		var instanced		= {};
		var extensions		= {};
		var dbs				= {};
		var moduleCounter	= 0;
		var docElements		= document.documentElement;
		var storageName;

		function createModuleId(name){
			var core = e.CORE;
			var moduleId = name + '-' + moduleCounter;
			moduleCounter++;
			return moduleId;
		}

		function canBeAService(name, service){
			return !services[name] && typeof service === 'object' || typeof service === 'function';
		}

		function createModule(temp, dependencies){
			var core 	= e.CORE;
			var util 	= core.util();
			var sandbox = core.sandbox();
			var dep, superName, serv;

			if(util.isString(dependencies)) {
				dep = dependencies.split(',');
				dep.forEach(function(serviceName){
					superName = serviceName.trim();
					serv = core.extend(superName)(sandbox.create( core, serviceName ));
					if(serv){
							for(var func in serv){
								if(serv.hasOwnProperty(func)){
									temp[superName] = temp[superName] || {};
									temp[superName][func] = serv[func].bind(temp);
								}
							}
					}
				});
			}
			return temp;
		}

		return {
			start:function( settings ){
				var core = e.CORE;
				return {
					run:function(){
						var self = this;
						self.startAllServices();
						if(settings && settings.storage){
								var storage = settings.storage;
								self.createStorage(storage);
								self.registerDomElements();
						}

						core.startAllModules(docElements);
					},
					registerDomElements:function(){
						var core = e.CORE;
						var binder 	= core.binder();
						binder.registerDomElements();
					},
					createStorage:function(storages){
						var name, type, storage;

						if(core.util().isArray(storages)){
							storages.forEach(function(db){
								name = db.name;
								type = db.type;
								if( name && type ){
									storage 	= e.CORE.storage();
									dbs = storage.setUp(name, type);
								} else {
									throw new Error('Storage parameters are missing {{name}}/{{type}}');
								}
							});
						} else {
							name = storages.name;
							type = storages.type;
							if( name && type ){
								dbs = storage.setUp(name, type);
							}
						}
					},
					//Start all services
					startAllServices:function(){
						var core = e.CORE;
						var service, dep, serv;
						for(var name in services){
								service = services[name];
								service.settings = settings;
								if(service.dependencies){
										dep = service.dependencies.split(',');
										dep.forEach(function(serviceName){
												serv = core[serviceName.trim()];
												if(serv){
														service[serviceName.trim()] = serv;
												} else {
														throw new Error('Dependency does not exist: ' + serviceName);
												}
										});
										delete service.dependencies;
								}
						}
					}
				};
			},
			//Select and start all classes
			startAllModules: function(root) {
				var core 		= e.CORE;
				var nodes 	= core.dom().element( MODULE_SELECTOR );
				var dbName = core.util().replace(MODULE_SELECTOR, '[,]', '');
				for(var i = 0; i < nodes.length; i++){
					var elem = core.dom().element(nodes[i]);
					core.startModule( elem, dbName );
				}
			},
			//Start module
			startModule:function( unex, dbName ){
				var core = e.CORE;
				var moduleId, createModule, element;
				var hasDomElement = core.util().isObject(unex);
				var moduleName = hasDomElement ? unex.attr(dbName) : unex;
				var m = modules[moduleName];

				if(m && !core.isStarted(m)){

					createModule = Object.create(m);
					moduleId  	 = createModuleId( moduleName );

					if(!instanced[moduleId]){
							if(hasDomElement){
									element = unex;
									unex.attr('id', moduleId);
									createModule.container 	= element;
							}
							createModule.id  		= moduleId;
							createModule.name 		= moduleName;
							instanced[moduleId] 	= createModule;
							instanced[moduleId].init();
					}

				} else {
					core.logger().error(moduleName + ' is, ' + m);
				}
			},
			//Get already started module
			getInstanceDataByModule:function(m){
				return instanced[m.moduleId];
			},
			//Check if module has already started
			isStarted:function(m){
				var core = e.CORE;
				return core.util().isObject(core.getInstanceDataByModule(m));
			},
			//Storage
			storage:function(){
				if(!storage){
					storage = e.CORE.service('storage');
				}
				return storage;
			},
			//Request
			request:function(){
				if(!request){
					request = e.CORE.service('request');
				}
				return request;
			},
			//Request
			validate:function(){
				if(!validate){
					validate = e.CORE.service('validate');
				}
				return validate;
			},
			//Util class
			dom:function(){
				if(!dom){
					dom = e.CORE.service('dom');
				}
				return dom;
			},
			//Util class
			socket:function(){
				if(!socket){
					socket = e.CORE.service('socket');
				}
				return socket;
			},
			//Util class
			logger:function(){
				if(!log){
					log = e.CORE.service('logger');
				}
				return log;
			},
			//Util class
			constant:function(name, value){
				if(!constant){
					constant = e.CORE.service('constant');
				}
				return constant;
			},
			//Util class
			sandbox:function(){
				if(!sandbox){
					sandbox = e.CORE.service('sandbox');
				}
				return sandbox;
			},
			//Util class
			util:function(){
				if(!util){
					util = e.CORE.service('util');
				}
				return util;
			},
			binder:function(){
				if(!binder){
					binder = e.CORE.service('binder');
				}
				return binder;
			},
			//Set extension
			extension:function(name, extension){
				if(!extensions[name]){
						extensions[name] = extension;
				} else {
					throw new Error('Extension does already exist: ' + name);
				}
			},
			//Get extension
			extend:function(name){
				var extension = extensions[name];
				if(extension){
					return extension;
				} else {
					throw new Error('Extensions does not exist: ' + name);
				}
			},
			//Get extension: Util, dom, socket, storage
			service:function(name){
				var service = services[name];
				if(service){
					return service;
				} else {
					throw new Error('Service does not exist: ' + name);
				}
			},
			//Set services: Util, dom, socket, storage
			define:function(name, service, dependencies){
				if(canBeAService(name, service)){
					if(dependencies) {
						service.dependencies = dependencies;
					}
					services[name] = (typeof service === 'object') ? service : service();
				}
			},
			//Notify {modules} event
			notify:function( eventName, data ){
				var evts = events[eventName];
				if(evts){
					evts.forEach(function( fn ){
						fn( data );
					});
				}
			},
			//Listen to event
			listen:function( eventName, fn ) {
				events[eventName] = events[eventName] || [];
				events[eventName].push(fn);
			},
			//Unlisten to event
			unlisten:function( eventName ) {
				if(events[eventName]){
					delete events[eventName];
				}
			},
			//Subscribe module
			subscribe:function( name, m, dependencies ){
				var core 	= e.CORE;
				var util 	= core.util();
				var sandbox = core.sandbox();
				var temp;

				if(util.isFunc(m) && util.isString(name)) {
					var func 	= sandbox.create( core, name );
					temp 	= m(func);

					if(temp.init && temp.destroy) {
						modules[name] = createModule(temp, dependencies);
					} else {
						throw new Error('Module is missing init or/and destroy: ' + m);
					}
					temp = null;
				}
			},
			bind:function( target, save ){
				var core 	= e.CORE;
				var binder 	= core.binder();
				var result 	= binder.bind( target );
				binder.notify( result.eventName, result.value );
				if(save){
					dbs.set( result.key, result.value );
				}
			},
			registerDomElement:function( element ){
					var core = e.CORE;
					var binder = core.binder();
					var namespace = binder.registerDomElement( element );
					var storageName = namespace.split('.');
					if(storageName.length > 1){
							var value = dbs.getByKey(storageName[1]);
							binder.notify( namespace, value );
					}
			},
			bindFromStorage:function(storageName){
				var core = e.CORE;
				var binder 	= core.binder();
				var s = dbs.read();
				for(var d in s){
					binder.notify( d, s[d] );
				}
			},
			readFromStorage:function(){
					if(dbs){
							return dbs;
					}
			},
			module:function(name){
				try {
					return modules[name];
				} catch (e) {
					this.logger().error(e.message);
				}
			}
		};
	}());

}( BEER ));
