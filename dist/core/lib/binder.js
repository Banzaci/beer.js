(function( e ){
    'use strict';
    e.CORE.define('binder', (function(){

        var elementEvents = {};
        var dataBind = 'data-bind';

        var tagNames = function( tagName ){
            if(tagName === 'input') {
                return 'value';
            }
            return 'html';
        }

        return {
            bind:function( target, notify ){
                var value = target.value;
                var db = target.getAttribute(dataBind);
                var eventName = db.replace(/-/g, '.');
                var args = db.split('-');
                var storageName = args.shift();
                var key = args.join('.');
                return { eventName:eventName, storageName:storageName, key:key, value:value };
            },
            notify:function(eventName, value){
                var evts = elementEvents[eventName];
                if(evts){
                    evts.forEach(function( element ){
                        element[tagNames(element.tagName().toLowerCase())](value);
                    });
                }
            },
            listen:function(nodeName, node){
                elementEvents[nodeName] = elementEvents[nodeName] || [];
                elementEvents[nodeName].push(node);
            },
            registerDomElement:function(element){
                var self = this;
                var nodeName = element.attr(dataBind).replace(/-/g, '.');
                self.listen(nodeName, element);
                return nodeName;
            },
            registerDomElements:function(){
                var self  = this;
                var nodes = self.dom().element( '[' + dataBind + ']' );
                for(var i = 0; i < nodes.length; i++){
					var elem = self.dom().element(nodes[i]);
					self.registerDomElement( elem );
				}
            }
        };
    }()), 'dom, logger');
}( BEER ));
