(function( e ){
    'use strict';
    e.CORE.define('constant', (function(){
        var constants = {};
        return {
            set:function(name, value){
                if(constants[name]){
                    this.logger.error('Exist');
                    return;
                }
                constants[name] = value;
            },
            get:function(name){
                if(!constants[name]){
                    this.logger.error('Not Exist');
                    return;
                }
                return constants[name];
            },
            exist:function(name){
                return constants[name] !== undefined;
            },
            clear:function(name){
                if(constants[name]){
                    delete constants[name];
                }
            }
        }

    }()));
}( BEER ));
