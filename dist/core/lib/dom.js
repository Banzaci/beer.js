(function( e ){
    'use strict';
    e.CORE.define('dom', (function(){
        var eventTypes = ['click', 'mouseover', 'mouseout', 'mousedown', 'mouseup',
			'mouseenter', 'mouseleave', 'keydown', 'keyup', 'submit', 'change',
			'contextmenu', 'dblclick', 'input', 'focusin', 'focusout', 'load',
            'mousemove'];

        var canAppendEventOnElement = function( evt, callback ){
            var util = this.util();
            return element !== null && util.isInArray(eventTypes, evt) && util.isFunc(callback);
        };

        var DomElement = function( params, parent ){

            var docElement = this;
            var i = 0;
            var nodes;
            var element = parent || document;

            if(params){

                if (window === this) {
                    return new DomElement( params );
                }

                if(typeof params === 'string') {
                    nodes =  element.querySelectorAll( params );
                    docElement.length = nodes.length;
                    for (; i < docElement.length; i++) {
                        docElement[i] = nodes[i];
                    }
                } else if(typeof params === 'object') {
                    docElement.length = 1;
                    docElement[i] = params;
                } else {
                    return;
                }
                return docElement;
            } else {
                return;
            }
        };

        DomElement.prototype = {
            doc:document.body,
            set:function(){
                var args = Array.prototype.slice.call(arguments);
                var fn   = args.shift();
                var func = this.doc[fn];
                var i    = this.length;

                if(typeof func === 'function'){
                    while (i--) {
                        this.item(i)[fn].apply(this.item(i), args);
                    }
                } else {
                    if(args.length === 1){
                        while (i--) {
                            this.item(i)[fn] = args[0];
                        }
                    } else if(args.length === 2) {
                        while (i--) {
                            this.item(i)[fn][args[0]] = args[1];
                        }
                    }
                }
            },
            get:function(){
                var args    = Array.prototype.slice.call(arguments);
                var fn      = args.shift();
                var func    = this.doc[fn];
                var len     = this.length;
                var i       = len;
                var values  = [];

                if(typeof func === 'function'){
                    while (i--) {
                        values[i] = this.item(i)[fn].apply(this.item(i), args);
                    }
                    return (values.length > 1) ? values : values[len-1];
                }
                return this.item(len-1)[fn];
            },
            prev:function(){
                return new DomElement( this.get('previousElementSibling') );
            },
            item:function(i){
                return this[i];
            },
            hasClass:function( className ){
                if ( this.item(0).nodeType === 1 && this.item(0).className.indexOf( className ) >= 0 ) {
                    return true;
                }
                return false;
            },
            addClass:function(className){
                var classNames = this.item(0).className;
                if(!this.hasClass(className)){
                    classNames += ' ' + className;
                    this.item(0).className = classNames;
                }
                return this;
            },
            removeClass:function(className){
                var classNames = this.item(0).className;
                if(this.hasClass(className)){
                    classNames = classNames.replace(className, '');
                    this.item(0).className = classNames;
                }
                return this;
            },
            removeAttribute:function(param){
                this.set('removeAttribute', param);
                return this;
            },
            view:function(){
                var hidden = 'hidden';
                var className = this.item(0).className;
                if(this.hasClass(hidden)){
                    className = className.replace(hidden, '');
                    this.item(0).className = className;
                }
                return this;
            },
            hide:function(){
                var hidden = 'hidden';
                if(!this.hasClass(hidden)){
                    this.item(0).className += ' ' + hidden;
                }
                return this;
            },
            toggle:function(){
                var hidden = 'hidden';
                var className = this.item(0).className;
                if(this.hasClass(hidden)){
                    className = className.replace(hidden, '');
                    this.item(0).className = className;
                } else {
                    className += ' ' + hidden;
                    this.item(0).className = className;
                }
                return this;
            },
            copy:function(){
              var child = new DomElement( this.item(0).cloneNode(true) );
              return child;
            },
            tagName:function(){
                this.get( 'tagName' );
                return this;
            },
            append:function( params ){
                this.set( 'appendChild', params.item(0) );
            },
            on : function( evt, callback ){
                this.set( 'addEventListener', evt, callback );
                return this;
            },
            off : function( evt, callback){
                this.set( 'removeEventListener', evt, callback );
                return this;
            },
            toggleDisabled:function(){
                if(this.attr('disabled')){
                    return this.set('removeAttribute', 'disabled');
                } else {
                    return this.attr('disabled', true);
                }
                return this;
            },
            attr:function( param, value ){
                if(value){
                    this.set( 'setAttribute', param, value );
                    return this;
                }
                return this.get( 'getAttribute', param );
            },
            html:function( value ){
               if(typeof value === 'string'){
                   this.set( 'innerHTML', value );
                   return this;
               } else {
                   return this.get( 'innerHTML' );
               }
           },
           isChecked:function(value){
             if(typeof value === 'string'){
                 this.set( 'checked', value );
                 return this;
             }
             return this.get( 'checked' );
           },
           selectedIndex:function(index){
              this.set( 'selectedIndex', index );
              return this;
           },
           empty:function( ){
              this.set( 'value', '' );
              return this;
           },
           value:function(value){
               if(typeof value === 'string'){
                   this.set( 'value', value );
                   return this;
               }
               return this.get( 'value' );
           },
           style:function( style, value ){
               this.set( 'style', style, value );
               return this;
           },
           createElement:function( param ){
               var child = new DomElement( document.createElement( param ) );
               this.item(0).appendChild(child.item(0));
               return child;
           },
           parent:function(){
              return new DomElement(this.item(0).parentNode);
           },
           remove:function(child){
             if(child){
                this.item(0).removeChild(child);
                return this;
             } else {
                this.item(0).parentNode.removeChild(this.item(0));
             }
           },
           context:function(context) {
              return this.item(0).getContext(context);
           },
           child:function(param){
             var prefix = param.substring(0, 1);
             var element;

             if(prefix === '#') {
                  element = document.getElementById(param.substring(1).toString());
                  return new DomElement( element );
             } else {
                element = this.item(0).getElementsByClassName(param);
                return new DomElement( element[0] );
             }
             return;
           },
           children:function(param){
             var elements = this.item(0).getElementsByClassName(param);
             if(elements.length > 0){
                  var arr = [];
                  for(var i=0; i<elements.length; i++){
                      arr.push(new DomElement( elements[i] ));
                  }
                  return arr;
             }
             return;
           },
           isNotEmpty:function(regLeng){
              var rg = regLeng || 0;
              return this.value().length > rg;
           },
           isEmail:function(){
             return /\S+@\S+\.\S+/.test(this.value());
           },
           isNumber:function(){
              return /^-?[\d.]+(?:e-?\d+)?$/.test(this.value());
           },
           toggleTime:function(className, time){
              var element = this.item(0);
              var elementClass = element.className;
              time = time || 1000;
              element.className += ' '+className;
              setTimeout(function(){
                  element.className = elementClass;
              }, time);
           }
       };

        return {
            element:function( params ){
                return new DomElement( params );
            }
        };

    }()));
}( BEER ));
