(function( e ){
    'use strict';
    e.CORE.define('logger', {
        error:function( message, name ){
            console.error('Message: ' + message + ', ' + name);
        },
        message:function( message, name ){
            console.log('Message: ' + message + ', ' + name);
        },
        toServer:function( message, url, fn ){}
    });
}( BEER ));
