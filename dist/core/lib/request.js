(function( e ){
	'use strict';
	e.CORE.define('request', (function(){

		function xhr( action, url ){
				var xhr, token;
				if (window.XMLHttpRequest) {
						xhr = new XMLHttpRequest();
				} else {
						xhr = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xhr.open( action, url, true );
				// if(token && token.id){
				// 		xhr.setRequestHeader('x-access-token', token.id);
				// }
				xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
				return xhr;
		}

		function stateChange( xhr, success, error ){
			xhr.addEventListener('readystatechange', function(data){
				if (xhr.readyState === 4){
					if(xhr.status === 200){
						success(xhr);
					} else {
						error(xhr.responseText);
					}
					xhr = null;
				}
			});
			return xhr;
		}

		function callHandler(type, evt, fn, data){
			var _xhr = xhr.call(this, type, evt);
			stateChange(_xhr, function(res){
				fn(JSON.parse(res.response));
			}, function(response){
				this.logger.error('error: ', response);
			}).send(JSON.stringify(data));
		}

		return {
			get:function(evt, fn, data){
				callHandler.call(this, 'get', evt, fn, data);
			},
			post:function(evt, fn, data){
				callHandler.call(this, 'post', evt, fn, data);
			},
			delete:function(evt, fn, data){
				callHandler.call(this, 'delete', evt, fn, data);
			},
			put:function(evt, fn, data){
				callHandler.call(this, 'put', evt, fn, data);
			}
		}
	}()), 'storage, logger');

}( BEER ));
