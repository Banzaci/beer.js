(function( e ){
    'use strict';
    e.CORE.define('storage', (function(){
        var storageObj = {};
        var fallBack = {
            storage:{},
            setItem:function(key, value){
                this.storage[key] = value;
            },
            getItem:function(key){
                return this.storage[key];
            }
        };

        function isSupported(typeOfStorage){
            try {
                if(typeOfStorage in window && window[typeOfStorage] !== null){
                    return window[typeOfStorage];
                } else {
                    return fallBack;
                }
            } catch(e) {
                return false;
            }
        }

        var storage = {
            setUp:function( typeOfStorage, storageName ){

                var storageType = isSupported(typeOfStorage);
                var util = this.util();

                if(storageObj[storageName]){
                  console.error('Name already exist!');
                  return;
                }

                storageObj[storageName] = {};

                return {
                    getByKey:function(key){
                        var storage = storageType.getItem(storageName);
                        if(storage){
                            return key.split('.').reduce(function (a, b) {
                              return a[b];
                            }, JSON.parse(storageType.getItem(storageName)));
                        }
                        return '';
                    },
                    set:function( key, value ){
                        var self    = this;
                        var temp    = {};
                        key.split('.').reduce(function(a, b, c, d) {
                            if (c === d.length - 1) {
                                a[b] = value;
                                return a[b];
                            }
                            return a[b] ? a[b] : a[b] = {};
                        }, temp);
                        storageType.setItem(storageName, JSON.stringify(util.collect(self.getAll(storageName), temp)));
                        temp = null;
                    },
                    getAll:function(){
                        return JSON.parse(storageType.getItem(storageName))
                    },
                    all:function(){},
                    deleteByKey:function(key){}
                };
            }
        };

        function rec( name, storageObj ){

            var nsObj = {};
            var dot = '.';
            var str = name + dot;

            return function run( reqObj ) {
                var obj = reqObj || storageObj;
                for(var o in obj){
                    if(typeof obj[o] === 'object'){
                        str += o + dot;
                        return run(obj[o]);
                    } else if (typeof obj[o] === "string") {
                        str += o;
                        nsObj[str] = obj[o];
                        str = name + dot;
                    }
                }
                return nsObj;
            }
        }

        return {
            setUp:function( storageName, typeOfStorage ){

                var self = this;
                var storageName = storageName;
                var storageApi  = storage.setUp.call(self, typeOfStorage, storageName);

                return {
                    getByKey:function( key ){
                        return storageApi.getByKey(key);
                    },
                    set:function(key, value){
                        storageApi.set(key, value);
                    },
                    getAll:function(){
                        return storageApi.getAll(storageName);
                    },
                    read:function( ){
                        var db = this.getAll();
                        var a = rec( storageName, db )();
                        return a;
                    }
                };
            }
        };
    }()), 'util, logger');
}( BEER ));
