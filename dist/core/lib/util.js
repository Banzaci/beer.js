(function( e ){
	'use strict';
	e.CORE.define('util', {
		list:function() {
			return Array.prototype.slice.call(arguments);
		},
		isInArray:function(arr, index){
			return arr.indexOf(index) > -1;
		},
		isArray:function( arr ){
	    	return (Object.prototype.toString.call(arr) === '[object Array]');
	    },
	    isFunc:function( obj ){
	    	return (typeof obj === 'function');
	    },
	    isPlainObject:function( obj ){
			return (Object.prototype.toString.call(obj) === '[object Object]');
	    },
	    isObject:function( obj ){
			return (typeof obj === 'object');
	    },
	    isString:function( str ){
			return (typeof str === 'string');
	    },
		fileExtension:function(filename) {
			return filename.split('.').pop();
		},
		toArray:function(args){
            var arr = [];
        	var i = 0;
            for(i; i<arr.length; i++){
                arr.push(args[i]);
            }
            return arr;
        },
		flatMap:function(arrs){
			var self = this;
			return arrs.reduceRight(function(a, b) {
	    		return self.concat(a, b);
			},[]);
		},
		collect:function() {
			var obj = {};
			var len = arguments.length;
			for (var i=0; i<len; i++) {
				for (var o in arguments[i]) {
					if (arguments[i].hasOwnProperty(o)) {
						obj[o] = arguments[i][o];
					}
				}
			}
			return obj;
		},
		replace:function(str, rep, newValue){
			var arr = rep.split(',');
			arr.forEach(function(value){
				str = str.replace(value, newValue);
			});
			return str;
		}
	});
}( BEER ));
