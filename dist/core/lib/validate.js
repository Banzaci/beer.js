(function( e ){
	'use strict';
	e.CORE.define('validate', function(){
			var validations = {
					min:function( value, req ){
							return value.length <= req;
					},
					max:function( value, req ){
							return value.length >= req;
					},
					email:function( value ){
							var emailFormat = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
							return !emailFormat.test( value );
					},
					date:function( value ){
							return isNaN(new Date(Date.parse(value)).getDate());
					},
					time:function( value ){
							var timeFormat = /^([0-9]{2})\:([0-9]{2})$/g;
							return !timeFormat.test( value );
					},
					equal:function( value, req ){
							var element = document.querySelectorAll(req);
							if(element.length > 0){
									return value !== document.querySelectorAll(req)[0].value;
							}
							return true;
					}
			};
      return {
          check:function( conditions, value, errors ){
              if(conditions.length === 0){
                  return errors;
              }
              var condition = conditions.shift();
              var funcs = condition.split(':');
              var func = funcs[0].trim();
              var req = (funcs.length > 1) ? funcs[1].trim() : '';
              var hasError = validations[func]( value, req );
              errors.push({ func:func, hasError:hasError });
              return this.check.call(this, conditions, value, errors);
          }
      };
	}());
}( BEER ));
