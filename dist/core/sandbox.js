(function( e ){
	'use strict';
	e.CORE.define('sandbox', {
		create:function( core, moduleName ) {
			return {
				module:function(name){
					return core.module(name);
				},
				service:function(serviceName){
					return core.service(serviceName);
				},
				bind:function( target, notify ){
					core.bind( target, notify );
				},
				storage:function( ){
					return core.readFromStorage( );
				},
				bindFromStorage:function( storageName ){
					core.bindFromStorage( storageName );
				},
				util:function(){
					return core.util();
				},
				request:function(){
					return core.request();
				},
				validate:function(){
					return core.validate();
				},
				socket:function(){
					return core.socket();
				},
				constant:function(){
					return core.constant();
				},
				extend:function(name){
					return core.extend(name);
				},
				unlisten:function( eventName ){
					core.unlisten( eventName );
				},
				listen:function( eventName, fn ) {
					core.listen( eventName, fn );
				},
				notify:function( eventName, data ){
					core.notify( eventName, data );
				},
				log:function(message){
					return core.logger().message(message, moduleName);
				},
				error:function(message){
					return core.logger().error(message, moduleName);
				},
				startModule:function(name, element){
					core.startModule(name, element);
				},
				registerDomElement:function( element ){
					core.registerDomElement(element);
				},
				element:function( selector, parent ){
					return core.dom().element( selector, parent );
				}
			};
		}
	});
}( BEER ));
