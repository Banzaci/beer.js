(function( e ){
    'use strict';
    e.CORE.define('socket', (function(){
        //need <script src="https://cdn.socket.io/socket.io-1.2.0.js"></script>
        var listenTo = [];
        var socket = (function() {
            return io.connect('http://www...')
                   .on('disconnect', function () {
                        console.log('- disconnected');
                    }).on('authenticated', function () {
                        console.log('- authenticated');
                    });
        }());

        socket.on('listen' , function ( response ) {
            if(response.eventName && listenTo[response.eventName]){
                listenTo[response.eventName].forEach(function(cb){
                    cb(response);
                });
            }
        });

        return {
          emit:function( data ){
              //In module: sandbox.service('socket').emit({ eventName:eventName, value:'Message to server!' });
              if(io && socket){
                  socket.emit('notify', data );
              }
          },
          on:function( eventName, cb ){
              //In module: sandbox.service('socket').on(eventName, function(result){});
              if(io && socket){
                  listenTo[eventName] = listenTo[eventName] || [];
                  listenTo[eventName].push(cb);
              }
          }
        };
    }));
}( BEER ));
