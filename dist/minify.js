#!/usr/local/bin/node
var files = [
    'dist/core/core.js',
    'dist/core/sandbox.js',
    'dist/core/lib/binder.js',
    'dist/core/lib/constant.js',
    'dist/core/lib/dom.js',
    'dist/core/lib/logger.js',
    'dist/core/lib/request.js',
    'dist/core/lib/storage.js',
    'dist/core/lib/util.js',
    'dist/core/lib/validate.js'
];

var option = {
    outSourceMap: "beer.min.js.map"
};

var fileDest  = 'beer.min.js';
var mapDest   = 'beer.min.js.map';
var _fs = require('fs');
var FILE_ENCODING = 'utf-8';
var uglyfyJS = require('uglify-js');

function minify(minDest, files, options){
  var result = uglyfyJS.minify(files, options);
  _fs.writeFileSync(minDest, result.code, FILE_ENCODING);
  _fs.writeFileSync(mapDest, result.map, FILE_ENCODING);
}

minify( fileDest, files, option );

console.log('Done with uglify!');
